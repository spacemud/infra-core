version: "3.7"

networks:
  traefik:
    external: true

services:
  traefik:
    image: "traefik:2.1"
    container_name: "traefik"
    restart: "unless-stopped"
    environment:
      - "CF_API_EMAIL=${EMAIL}"
      - "CF_DNS_API_TOKEN=${CLOUDFLARE_DDNS_API_TOKEN}"
    command:
      - "--log.level=DEBUG"
      - "--api.dashboard=true"
      - "--providers.file.directory=/data"
      - "--providers.file.watch=true"
      - "--providers.docker=true"
      - "--providers.docker.exposedByDefault=false"
      - "--providers.docker.network=traefik"
      - "--entrypoints.http.address=:80"
      - "--entrypoints.https.address=:443"
      - "--certificatesResolvers.letsencrypt.acme.dnsChallenge=true"
      - "--certificatesResolvers.letsencrypt.acme.dnsChallenge.provider=cloudflare"
      - "--certificatesResolvers.letsencrypt.acme.dnsChallenge.resolvers=1.1.1.1:53,1.0.0.1:53"
      - "--certificatesResolvers.letsencrypt.acme.dnsChallenge.delayBeforeCheck=0"
      - "--certificatesResolvers.letsencrypt.acme.email=${EMAIL}"
      - "--certificatesResolvers.letsencrypt.acme.storage=/data/acme.json"
    volumes:
      - "./traefik:/data"
      - "/var/run/docker.sock:/var/run/docker.sock:ro"
    ports:
      - "80:80"
      - "443:443"
    networks:
      - "traefik"
    labels: 
      - "traefik.enable=true"
      # Authentication
      - "traefik.http.routers.traefik.middlewares=forwardAuth@file,secureHeaders@file"
      # Reverse proxy
      - "traefik.http.routers.traefik.rule=Host(`traefik.${SUBDOMAIN}.${DOMAIN}`)"
      - "traefik.http.routers.traefik.entrypoints=https"
      - "traefik.http.routers.traefik.service=api@internal"
      - "traefik.http.routers.traefik.tls=true"
      - "traefik.http.routers.traefik.tls.certresolver=letsencrypt"
      - "traefik.http.routers.traefik.tls.domains[0].main=${SUBDOMAIN}.${DOMAIN}"
      - "traefik.http.routers.traefik.tls.domains[0].sans=*.${SUBDOMAIN}.${DOMAIN}"
      # HTTP redirection
      - "traefik.http.routers.http_catchall.rule=HostRegexp(`{host:.+}`)"
      - "traefik.http.routers.http_catchall.entrypoints=http"
      - "traefik.http.routers.http_catchall.middlewares=https_redirect"
      - "traefik.http.middlewares.https_redirect.redirectscheme.scheme=https"
      - "traefik.http.middlewares.https_redirect.redirectscheme.permanent=true"

  traefik-forward-auth:
    image: "thomseddon/traefik-forward-auth:2"
    container_name: "traefik-forward-auth"
    environment:
      - "PROVIDERS_GOOGLE_CLIENT_ID=${PROVIDERS_GOOGLE_CLIENT_ID}"
      - "PROVIDERS_GOOGLE_CLIENT_SECRET=${PROVIDERS_GOOGLE_CLIENT_SECRET}"
      - "SECRET=${SECRET}"
      - "AUTH_HOST=auth.${SUBDOMAIN}.${DOMAIN}"
      - "COOKIE_DOMAIN=${SUBDOMAIN}.${DOMAIN}"
      - "WHITELIST=${WHITELIST}"
    networks:
      - "traefik"
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.traefik-forward-auth.middlewares=forwardAuth@file,secureHeaders@file"
      - "traefik.http.routers.traefik-forward-auth.rule=Host(`auth.${SUBDOMAIN}.${DOMAIN}`)"
      - "traefik.http.services.traefik-forward-auth.loadbalancer.server.port=4181"
      - "traefik.http.routers.traefik-forward-auth.entrypoints=https"
      - "traefik.http.routers.traefik-forward-auth.tls.certresolver=letsencrypt"

  whoami:
    image: "containous/whoami"
    container_name: "whoami"
    networks:
      - "traefik"
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.whoami.middlewares=forwardAuth@file,secureHeaders@file"
      - "traefik.http.routers.whoami.rule=Host(`whoami.${SUBDOMAIN}.${DOMAIN}`)"
      - "traefik.http.routers.whoami.entrypoints=https"
      - "traefik.http.routers.whoami.tls.certresolver=letsencrypt"

  portainer:
    image: "portainer/portainer:latest"
    container_name: "portainer"
    restart: "unless-stopped"
    command:
      - "--no-auth"
    volumes:
      - "./portainer:/data"
      - "/var/run/docker.sock:/var/run/docker.sock:ro"
    networks:
      - "traefik"
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.portainer.middlewares=forwardAuth@file,secureHeaders@file"
      - "traefik.http.routers.portainer.rule=Host(`portainer.${SUBDOMAIN}.${DOMAIN}`)"
      - "traefik.http.routers.portainer.entrypoints=https"
      - "traefik.http.routers.portainer.tls.certresolver=letsencrypt"
